<?php
// Variables
$sum = 0;
$price = 0;
// The input variable allows the user to input data.
echo("        -----MENU-----
        -ITEM---PRICE-

         Coke - 1.00
         Soda - 0.80
         Crisps - 1.5
         Water - 0.70
         Nuts - 2.00
        --------------

This machine only accepts British tender.
Only change is accepted.
Once you have entered your amount, type Start and then type the item you want and hit enter.
0.1, 0.2, 0.5, 1, and 2

Please Insert Coins Here: ");
// The readline reads a single line from the user, this being the amount they are inputing.
$input = readline();

// The while loop, checks for the users input, the inputs have been limited to real world currency.
while ($input !== 'Start') {
// Below are the inputs that can be used.
    if ($input == '0.1' || $input == '0.2' || $input == '0.5' || $input == '1' || $input == '2') {
        $sum += round($input, 2);
    } else {
        echo 'Cannot accept, please enter one of the ammounts displayed above. ' . $input . PHP_EOL;
        // The else statement will display an error message if the user tries to input any numbers that arent displayed.
    }

    $input = readline("Please type Start when you are done: ");
    // This readline waits for the users input, which should be either more money, or the Start command.
}
// This readline waits for the user to enter there desired item.
$input = readline("Please enter your desired item: ");



while ($input !== 'End') {
// This is the list of products, when the users enters there desired product the price is saved.
    switch ($input) {
        case 'Nuts':
            $price = 2.00;
            break;
        case 'Water':
            $price = 0.70;
            break;
        case 'Crisps':
            $price = 1.5;
            break;
        case 'Soda':
            $price = 0.80;
            break;
        case 'Coke':
            $price = 1.00;
            break;
        default:
        // This will appear if the user has not entered a valid product.
            echo 'Product Not Listed' . PHP_EOL;
            $price = 0;
    }
// If the user has chosen a product and doesnt have enough money for it this message will be displayed.
    if ($sum < $price) {
        echo 'Sorry, not enough money' . PHP_EOL;
    } else if ($price !== 0) {
        $sum -= $price;
        // This echo tells the user if they have purchased the items aswell as telling them how to receive their change and add more items.
        echo 'Purchased ' . strtolower($input) . PHP_EOL;
    }
  //  if ($input !== "Refund") {
  //     printf $sum;
  //   }
// This readline waits for the user to enter another item, or use the End command, to receive their change.
    $input = readline("Please enter anymore desired items, otherwise type End to get your change: ");


}



// This prints the users change.
printf("Change: %.2f", $sum);
